# What is this repository for? #

* The contents of this repository will set up an ESP8266 with a file system so it can serve webpages from the file system.  This eliminates the need for writing the bulk of html, css, and javascript within an arduino sketch.
* Version 1.0 - initial commit to this repository will allow web controls to update the status of pins on an ESP8266 and also allow the ESP8266 to update the webpage dynamically using Ajax

# How do I get set up? #
If you have not done so previously, you will need to modify your Arduino IDE so it is capable of writing to the flash memory of an ESP8266 module.  
Instruction to do this can be found [here](http://arduino.esp8266.com/versions/1.6.5-1160-gef26c5f/doc/reference.html#uploading-files-to-file-system).
Instead of using the zip file they provide, you may need to use the zip file within this repository instead.  I was having issues initializing the file system using their zip file.  The one provided here works for me on an Adafruit Huzzah.

##Credit where credit is due##
This repository is a modified version from the tutorial provided by [Starting Electronic](https://startingelectronics.org/tutorials/arduino/ethernet-shield-web-server-tutorial).  Here they use an ethernet shield instead of an ESP8266 module.