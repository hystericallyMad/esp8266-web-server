#include <FS.h>
#include <ESP8266WiFi.h>

const char* ssid = "YOUR WIFI SSID HERE";
const char* password = "YOUR WIFI PASSWORD HERE";
int LED_PIN = 2;
int BTN_PIN = 16;
int cnt = 0;
int BUF_SIZE = 1000;
WiFiServer server(80);

String HTTP_req;
boolean LED_status = false;


File webFile;
//Method - Function Declarations
void Process_HTTP_Req(WiFiClient);
void SendXML(WiFiClient);
void SendFile(WiFiClient,String);
String ContentType(String);
void FileStatus();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  delay(10);
  pinMode(LED_PIN,OUTPUT);
  digitalWrite(LED_PIN, LOW);
  pinMode(BTN_PIN,INPUT);

  //Connect to wifi network
  Serial.println();
  Serial.println();
  Serial.println("Connecting to: ");
  Serial.println(ssid);
  WiFi.begin(ssid,password);
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("Wifi connected");

  //Start Web Server
  server.begin();
  Serial.println("Server Started");

  //Print Web Server IP Address
  Serial.print("Use this URL to connect: ");
  Serial.println("http://" + WiFi.localIP().toString() + "/");

  //Mount File System
  SPIFFS.begin();
  if (!SPIFFS.exists("/index.htm")){
    Serial.println("ERROR - Can't find index.htm file!");
    return;
  }
  Serial.println("SUCCESS - Found index.htm file!");
}

void loop() {
  // put your main code here, to run repeatedly:
  //Check hardware button status
  if (digitalRead(BTN_PIN)){
    if (LED_status == true){
      LED_status = false;
      digitalWrite(LED_PIN,LOW);
    }else{
      LED_status = true;
      digitalWrite(LED_PIN,HIGH);
    }
  }
  //Check for Web Client
  WiFiClient client = server.available();
  if (!client){
    return;
  }
  Serial.println("Client Connected");
  while(!client.available()){
    delay(1);
  }
  HTTP_req = client.readStringUntil(0x13);
  client.flush();

  Serial.println("HTTP_req: " + HTTP_req);
  Process_HTTP_Req(client);
  
}
void Process_HTTP_Req(WiFiClient client){
  if (HTTP_req.indexOf("GET / HTTP/1.1")!= -1){
    //Send the initial webpage to the client
    SendFile(client,"/index.htm");
  }
  else if (HTTP_req.indexOf("whiteLED.png") != -1){
    //Send whiteLED.png
    SendFile(client,"/whiteLED.png");
  }
  else if (HTTP_req.indexOf("blueLED.png") != -1){
    //Send blueLED.png
    SendFile(client,"/blueLED.png");
  }
  else if(HTTP_req.indexOf("LED1=1") != -1){
    //Turn on LED
    digitalWrite(LED_PIN,HIGH);
    LED_status = true;
    SendXML(client);
  }
  else if(HTTP_req.indexOf("LED1=0") != -1){
    //Turn off LED
    digitalWrite(LED_PIN,LOW);
    LED_status = false;
    SendXML(client);
  }
  else if(HTTP_req.indexOf("ajax_input") != -1){
    //Send XML file to update webpage widgets
    SendXML(client);
  }
}
void SendXML(WiFiClient client){
  client.print("<?xml version = '1.0' ?>");
  client.print("<inputs>");
  //Get LED Status
  client.print("<LED>");
  if (LED_status){
    client.print("on");
  }
  else{
    client.print("off");
  }
  client.println("</LED>");
  //Get Switch Status
  client.print("<Switch>");
  if (digitalRead(BTN_PIN)){
    client.print("on");
  }
  else{
    client.print("off");
  }
  client.println("</Switch>");
  client.print("</inputs>");
}
void SendFile(WiFiClient client,String fileName){
  Serial.print("Sending file: ");
  Serial.println(fileName);
  client.println("HTTP/1.1 200 OK");
  client.println(ContentType(fileName));
  client.println("Connection: close");
  client.println();
  webFile = SPIFFS.open(fileName,"r");
  if (webFile){
     // while(webFile.available()){
     //   client.write(webFile.read());
     //   FileStatus();
      //}
      Serial.print("File size: ");
      Serial.println(webFile.size());
      byte buf[BUF_SIZE];
      
      int i = 0;
      while(webFile.available()){
        buf[i] = webFile.read();
        if (i == (BUF_SIZE -1 )){
          client.write(buf,i +1);
          Serial.print("Sending: ");
          Serial.print(i);
          Serial.println(" bytes");
          for (int ii = 0; ii <= BUF_SIZE; ii++){
            buf[ii] = 0;
          }
          i = 0;
        }
        else{
          i++;          
        }
      }
      client.write(buf,i +1);
      Serial.print("Sending: ");
      Serial.print(i);
      Serial.println(" bytes");
      webFile.close();
      Serial.println();
    }
  Serial.print("File sent: ");
  Serial.println(fileName);   
}
String ContentType(String fileName){
  String contentType = "Content-Type: ";
  if (fileName.indexOf("htm") != -1){
   contentType += "text/html";
  }
  else if (fileName.indexOf("png") != -1){
    contentType += "image/png";
  }
  Serial.println(contentType);
  return contentType;
}
void FileStatus(){
  if (cnt % 100 == 0){
    Serial.print(".");
  }
  cnt += 1;
}



















